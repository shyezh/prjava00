import java.net.*;

/**
 * 
 * @author shyezh1617daw2@gmail.com
 */

public class prjava00 {

        public static void main(String[] args){
            System.out.println("Hola Mon");
            System.out.println("Versio 0.1 del projecte prjava00");
            try {
            InetAddress addr = InetAddress.getLocalHost();
            String ipAddr = addr.getHostAddress();
            String hostname = addr.getHostName();
                System.out.println("hostname="+hostname);
                System.out.println("Adreça IP: " + ipAddr);
                System.out.println("nom de l'usuari: " + System.getProperty("user.name"));
                System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
                System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
                System.out.println("Versió OS: " + System.getProperty("os.version"));
            }
            catch (UnknownHostException e) {
                e.printStackTrace();                
            }
        }
}
